SHELL := /bin/bash

CODE_DIR=build/code
DOC_DIR=build/docs
SRC_DIR=src
PWD=$(shell pwd)
EXP_DIR=/home/travula/projects/vxcg/pub/orgmode/html-themes/experiment
SORT_DIR=/home/travula/projects/vlead-projects/gitlab/experiments/mergesort/mergesort-demo
STATUS=0

all:  build

init: 
	./init.sh

build: init
	make -f tangle-make -k all exptheme=true

run:
	(cd ${DOC_DIR}; python -m SimpleHTTPServer)

clean:	
	make -f tangle-make clean

copy:
	(rsync -a ${EXP_DIR}/style/js/ ${SORT_DIR}/literate-tools/style/js/; rsync -a ${EXP_DIR}/org-templates/ ${SORT_DIR}/literate-tools/org-templates/exptheme/)
